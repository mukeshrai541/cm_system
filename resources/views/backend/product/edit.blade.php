@extends('backend.layouts.master')
@section('title')
Product Edit
@endsection

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-12">
                @include('flashMessage.message')
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <form action="{{ route('product.update', $product->id) }}" method="post">
            @csrf
            @method('put')
            <div class="row">
                <div class="col-md-9">
                    <div class="card card-primary card-tabs">
                        <div class="card-header">
                            <h3 class="card-title">Product</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="name">Product Name</label>
                                <input type="text" id="name" class="form-control" name="name"
                                    value="{{ old('name')?old('name'):$product->name }}">
                                @if($errors->has('name'))
                                <span class="text-danger">
                                    {{ $errors->first('name') }}
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="price">Product Price (Rs)</label>
                                <input type="text" id="price" class="form-control" name="price"
                                    value="{{ old('price')?old('price'):$product->price }}">
                                @if($errors->has('price'))
                                <span class="text-danger">
                                    {{ $errors->first('price') }}
                                </span>
                                @endif
                            </div>
                                                      
                            <div class="form-group">
                                <div class="input-group-prepend">
                                    <label for="product_category_id">Category</label>
                                </div>
                                <select class="custom-select" id="product_category_id" name="product_category_id">
                                    <option>Choose product categories below...</option>
                                    @foreach ($product_categories as $item)
                                    <option value="{{ $item->id }}" {{ (($item->id==$product->product_category_id)?'selected':'') }}>
                                        {{ ucfirst($item->name) }} 
                                    </option>
                                    @endforeach
                                </select>
                                @if($errors->has('product_category_id'))
                                <span class="text-danger">
                                    Please select the category.
                                </span>
                                @endif
                            </div>
                        </div>
                        <!-- /.card -->
                    </div>
                </div>

                <div class="col-md-3">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Action</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Submit">
                                <a href="{{ route('product.index') }}" class="btn btn-danger">Close</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection