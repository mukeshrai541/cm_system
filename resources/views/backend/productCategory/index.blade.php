@extends('backend.layouts.master')

@section('title')
Manage Category
@endsection
@section('content')
<section class="content">
    @include('flashMessage.message')
    <div class="card">
        <div class="card-header">
            Categories
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">S.N</th>
                        <th scope="col">Category</th>
                        <th scope="col">Order</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($productCategory as $value)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$value->name}}</td>
                        <td>{{$value->order}}</td>
                        <td>
                            <form class="form-inline" method="post"
                                action="{{ route('product_category.destroy', $value->id) }}">
                                @csrf
                                @method('delete')
                                <a href="{{ route('product_category.edit', $value->id) }}" class="btn btn-primary m-2 btn-sm"><i
                                        class="fa fa-edit"> </i></a>
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm">                                                                                
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">
                            No data found!
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</section>
@stop
