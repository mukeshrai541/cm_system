@extends('backend.layouts.master')
@section('title')
Customer by location
@endsection

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Form</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form role="form">
                        <div class="form-group">
                            <label for="all_location">Product Category</label>
                            <select class="form-control"  id="all_location" name="all_location">
                                <option selected="selected" value="">Select any category below...</option>
                                @for ($i = 0; $i < count( $all_location ); $i++)
                                <option value="{{ $all_location[$i] }}">{{ $all_location[$i] }}</option>                                    
                                @endfor    
                            </select>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Customers By Location</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="packages_table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th width="5%">
                                    S.N
                                </th>
                                <th width="15%">
                                    Full Name
                                </th>
                                <th width="15%">
                                    Email
                                </th>
                                <th width="5% ">
                                    Phone Number
                                </th>
                                <th width="15%">
                                    Date of Birth
                                </th>
                                <th width="15%">
                                    Address
                                </th>
                                <th width="15%">
                                    Created At
                                </th>
                                <th width="20% ">
                                    Action
                                </th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
    <!-- /.row -->
</div><!-- /.container-fluid -->
@endsection

@section('script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready( function () {
        $('#all_location').select2({
            theme:'bootstrap4'
        });       

        $('#all_location').on('change', function(e){
            var location = $('#all_location').val();
            $('#packages_table').DataTable().destroy();
            var dataTable = $('#packages_table').DataTable({
                processing: true,
                serverSide: true,
                ajax:{
                    url: "{{ route('ajax.customer_by_location') }}",
                    type: "post",
                    dataType:"json",
                    data:{
                        location:location
                    }
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex' 
                    },
                    {
                        data:'full_name',
                        name:'full_name'
                    },
                    {
                        data:'email',
                        name:'email'
                    },
                    {
                        data:'phone_number',
                        name:'phone_number'
                    },
                    {
                        data:'dob',
                        name:'dob'
                    },
                    {
                        data:'address',
                        name:'address'
                    },
                    {
                        data:'created_at',
                        name:'created_at'
                    },
                    {
                        data:'action',
                        name:'action'
                    }
                ]
            });
        });
    });  
</script>
@endsection