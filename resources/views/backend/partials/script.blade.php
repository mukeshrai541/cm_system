<!-- jQuery -->
<script src="{{asset('adminlte/js/jquery.min.js')}}"></script>
{{-- <script src="{{asset('jquery.min.js')}}"></script> --}}

<!-- Bootstrap 4 -->
<script src="{{asset('adminlte/js/bootstrap.min.js')}}"></script>
{{-- <script src="{{asset('bootstrap-4.5.0-dist/js/bootstrap.min.js')}}"></script> --}}

<!-- AdminLTE App -->
<script src="{{asset('adminlte/js/adminlte.min.js')}}"></script>

{{-- font awesome --}}
<script src="{{asset('adminlte/js/fontawesome.js')}}"></script>

{{-- jquery slim --}}
{{-- <script type="text/javascript" src="{{asset('adminlte/js/jquery_slim.min.js')}}"></script> --}}

{{-- popper js --}}
<script type="text/javascript" src="{{asset('adminlte/js/popper.min.js')}}"></script>


{{--ckeditor--}}
<script type="text/javascript" src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>

{{-- data table --}}
<script src="{{ asset('jQuery/jquery-3.3.1.js') }}"></script>
<script src="{{ asset('dataTable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('dataTable/js/dataTables.bootstrap4.min.js') }}"></script>

{{-- select2 --}}
<script src="{{ asset('select2/dist/js/select2.min.js') }}"></script>