<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="csrf-token" content="{{ csrf_token() }}">


<title>
    Sales Recording System | @yield('title')
</title>

{{-- adminlte css --}}
<link rel="stylesheet" href="{{asset('adminlte/css/adminlte.min.css')}}">


<link rel="stylesheet" type="text/css" href="{{asset('adminlte/css/custom.css')}}">

<link rel="stylesheet" href="{{ asset('frontend/fontawesome-new/css/all.css') }}">

<link rel="stylesheet" href="{{asset('adminlte/css/bootstrap.min.css')}}">
{{-- <link rel="stylesheet" href="{{asset('bootstrap-4.5.0-dist/css/bootstrap.min.css')}}"> --}}

{{-- data table --}}
<link href="{{ asset('dataTable/css/jquery.dataTables.min.css') }}" rel="stylesheet">

{{-- select 2 --}}
<link href="{{ asset('select2/dist/css/select2.min.css') }}" rel="stylesheet" />
