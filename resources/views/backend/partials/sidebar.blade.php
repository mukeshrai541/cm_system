<aside class="main-sidebar sidebar-dark-primary elevation-4">

  <!-- Brand Logo -->

  <a href="{{ route('home') }}" class="brand-link" target="1" <span
    class="brand-text font-weight-light">Sales Recording System</span>

  </a>



  <!-- Sidebar -->

  <div class="sidebar">

    <!-- Sidebar Menu -->

    <nav class="mt-2">

      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-header">
          {{ ucfirst(Auth::user()->name) }}
        </li>
        
        <li class="nav-header">Menu</li>
        
        
        <li class="nav-item has-treeview ">
          <a href="{{ route('home') }}" class="nav-link {{ (request()->routeIs('home')) ? 'active' : '' }}">

            <i class="fas fa-tachometer-alt"></i>

            <p>

              Dashboard

            </p>

          </a>
        </li>        
        
        
        <li class="nav-item has-treeview {{ (request()->routeIs('customer.*')) ? 'menu-open' : '' }}">

          <a href="#" class="nav-link {{ (request()->routeIs('customer.*')) ? 'active' : '' }}">

            <i class="fas fa-users"></i>

            <p>

              Customer

            </p>
            <i class="fas fa-angle-left right"></i>

          </a>

          <ul class="nav nav-treeview">

            <li class="nav-item ">

              <a href="{{ route('customer.create') }}" class="nav-link {{ (request()->routeIs('customer.create')) ? 'active' : '' }}">

                <i class="fa fa-circle nav-icon"></i>

                <p>Add New Customer</p>

              </a>

            </li>

            <li class="nav-item">

              <a href="{{ route('customer.index') }}" class="nav-link {{ (request()->routeIs('customer.index')) ? 'active' : '' }}">

                <i class="fa fa-circle nav-icon"></i>

                <p>Manage Customer</p>

              </a>

            </li>

          </ul>

        </li>        
        
        <li class="nav-item has-treeview {{ (request()->routeIs('product_category.*')) ? 'menu-open' : '' }}">
          
          <a href="#" class="nav-link {{ (request()->routeIs('product_category.*')) ? 'active' : '' }}">

            <i class="fas fa-puzzle-piece"></i>

            <p>

              Product Category

            </p>
            <i class="fas fa-angle-left right"></i>

          </a>

          <ul class="nav nav-treeview">

            <li class="nav-item">

              <a href="{{ route('product_category.create') }}" class="nav-link {{ (request()->routeIs('product_category.create')) ? 'active' : '' }}">

                <i class="fa fa-circle nav-icon"></i>

                <p>Add New Category</p>

              </a>

            </li>

            <li class="nav-item">

              <a href="{{ route('product_category.index') }}" class="nav-link {{ (request()->routeIs('product_category.index')) ? 'active' : '' }}">

                <i class="fa fa-circle nav-icon"></i>

                <p>Manage Category</p>

              </a>

            </li>

          </ul>

        </li>

        <li class="nav-item has-treeview {{ (request()->routeIs('product.*')) ? 'menu-open' : '' }}">

          <a href="#" class="nav-link {{ (request()->routeIs('product.*')) ? 'active' : '' }}">

            <i class="fas fa-box-open"></i>

            <p>

              Products

            </p>
            <i class="fas fa-angle-left right"></i>

          </a>

          <ul class="nav nav-treeview">

            <li class="nav-item">

              <a href="{{ route('product.create') }}" class="nav-link {{ (request()->routeIs('product.create')) ? 'active' : '' }}">

                <i class="fa fa-circle nav-icon"></i>

                <p>Add New Product</p>

              </a>

            </li>

            <li class="nav-item">

              <a href="{{ route('product.index') }}" class="nav-link {{ (request()->routeIs('product.index')) ? 'active' : '' }}">

                <i class="fa fa-circle nav-icon"></i>

                <p>Manage Product</p>

              </a>

            </li>

          </ul>

        </li> 
        
        <li class="nav-item has-treeview {{ (request()->routeIs('sale.*')) ? 'menu-open' : '' }}">

          <a href="#" class="nav-link  {{ (request()->routeIs('sale.*')) ? 'active' : '' }}">

            <i class="fas fa-coins"></i>

            <p>

              Sales

            </p>
            <i class="fas fa-angle-left right"></i>

          </a>

          <ul class="nav nav-treeview">

            <li class="nav-item">

              <a href="{{ route('sale.create') }}" class="nav-link {{ (request()->routeIs('sale.create')) ? 'active' : '' }}">

                <i class="fa fa-circle nav-icon"></i>

                <p>Add New Sales</p>

              </a>

            </li>

            <li class="nav-item">

              <a href="{{ route('sale.index') }}" class="nav-link {{ (request()->routeIs('sale.index')) ? 'active' : '' }}">

                <i class="fa fa-circle nav-icon"></i>

                <p>Manage Sales</p>

              </a>

            </li>

          </ul>

        </li> 

        @role('admin')
        <li class="nav-item has-treeview">
          <a href="{{ route('customer_by_location.index') }}" class="nav-link {{ (request()->routeIs('customer_by_location.index')) ? 'active' : '' }}">

            <i class="fas fa-map-marker-alt"></i>

            <p>

              Customer By Location

            </p>

          </a>
        </li>

        <li class="nav-item has-treeview {{ (request()->routeIs('user.*')) ? 'menu-open' : '' }}">

          <a href="#" class="nav-link {{ (request()->routeIs('user.*')) ? 'active' : '' }}">

            <i class="fas fa-user-tie"></i>

            <p>

              User

            </p>
            <i class="fas fa-angle-left right"></i>

          </a>

          <ul class="nav nav-treeview">

            <li class="nav-item">

              <a href="{{ route('user.create') }}" class="nav-link {{ (request()->routeIs('user.create')) ? 'active' : '' }}">

                <i class="fa fa-circle nav-icon"></i>

                <p>Add User</p>

              </a>

            </li>

            <li class="nav-item">

              <a href="{{ route('user.index') }}" class="nav-link {{ (request()->routeIs('user.index')) ? 'active' : '' }}">

                <i class="fa fa-circle nav-icon"></i>

                <p>Manage User</p>

              </a>

            </li>

          </ul>

        </li>
        @endrole
      </ul>

    </nav>

    <!-- /.sidebar-menu -->

  </div>

  <!-- /.sidebar -->

</aside>