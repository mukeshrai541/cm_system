@extends('backend.layouts.master')
@section('title')
Sale
@endsection

@section('custom_css')
<link href="{{ asset('dataTable/css/jquery.dataTables.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-1">
            <div class="col-sm-12">
                @include('flashMessage.message')
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Customers</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="packages_table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%">
                                        S.N
                                    </th>
                                    <th width="20%">
                                        Customer Name
                                    </th>
                                    <th width="20%">
                                        Product
                                    </th>
                                    <th width="10%">
                                        Price(Rs)
                                    </th>
                                    <th width="5%">
                                        Quantity
                                    </th>
                                    <th width="20%">
                                        Transaction Date    
                                    </th>
                                    <th width="20% ">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->

    <div class="modal" id="deletePackageModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <form action="" method="post" id="deleteForm">
                    <div class="modal-header">
                        <h4 class="modal-title">Delete Customer</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @csrf
                        @method('delete')
                        Are you sure you want to delete this customer?
                    </div>
                    <div class=" modal-footer justify-content-between">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger" data-dismiss="modal"
                            onclick="formSubmit()">Yes</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection

@section('script')
<script src="{{ asset('jQuery/jquery-3.3.1.js') }}"></script>
<script src="{{ asset('dataTable/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('dataTable/js/dataTables.bootstrap4.min.js') }}"></script>

<script>
    $(document).ready( function () {
        $('#packages_table').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('sale.index') }}",
            columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex' },
                { data: 'customer_name', name: 'customer_name' },
                // { data: 'category', name: 'category' },
                { data: 'product_name', name: 'product_name' },
                { data: 'price', name: 'price' },
                { data: 'quantity', name: 'quantity' },
                { data: 'created_at', name: 'created_at' },
                { data: 'action', name: 'action', orderable:false },
            ]
        });
    });  

    function deleteData(id)
    {
        var id = id;
        var url = '{{ route("sale.destroy", ":id") }}';
        url = url.replace(':id', id);
        $("#deleteForm").attr('action', url);
    }

    function formSubmit()
    {
        $("#deleteForm").submit();
    }     
</script>
@endsection