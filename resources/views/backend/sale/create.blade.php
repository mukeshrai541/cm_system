@extends('backend.layouts.master')
@section('title')
Add New Sales
@endsection

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-1">
            <div class="col-sm-12">
                @include('flashMessage.message')
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
    <div class="container-fluid">
        <form action="{{ route('sale.store') }}" method="post">
            @csrf
            <div class="row">
                <div class="col-md-9">
                    <div class="card card-primary card-tabs">
                        <div class="card-header">
                            <h3 class="card-title">Sale</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="customer_name">Customer Name</label>
                                <select class="form-control"  id="customer_name" name="customer_name">
                                    <option selected="selected" value="">Choose customer below...</option>
                                    @foreach ($customer as $item)
                                        <option value="{{ $item->full_name }}" {{ (old('customer_name')==$item->full_name)?'selected':'' }}>{{ $item->full_name }}</option>
                                    @endforeach
                                </select>
                                @if($errors->has('customer_name'))
                                <span class="text-danger">
                                    {{ $errors->first('customer_name') }}
                                </span>
                                @endif
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="product_category">Product Category</label>
                                        <select class="form-control"  id="product_category" name="product_category">
                                            <option selected="selected" value="">Select any category below...</option>
                                            @foreach ($product_category as $item)
                                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('product_category'))
                                        <span class="text-danger">
                                            {{ $errors->first('product_category') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="product_id">Product Name</label>
                                        <select class="form-control"  id="product_id" name="product_id">
                                            <option selected="selected" value="">Choose product below...</option>
                                        </select>
                                        @if($errors->has('product_id'))
                                        <span class="text-danger">
                                            {{ $errors->first('product_id') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>                                                        

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="price">Product Price (Rs)</label>
                                        <input type="text" id="price" class="form-control" name="price"
                                            value="{{ old('price')?old('price'):'' }}" placeholder="XXX.XX">
                                        @if($errors->has('price'))
                                        <span class="text-danger">
                                            {{ $errors->first('price') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="quantity">Quantity (Rs)</label>
                                        <input type="number" id="quantity" class="form-control" name="quantity"
                                            value="{{ old('quantity')?old('quantity'):'' }}" placeholder="X">
                                        @if($errors->has('quantity'))
                                        <span class="text-danger">
                                            {{ $errors->first('quantity') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>                                                                                

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="amount">Amout (Rs)</label>
                                        <input type="number" id="amount" class="form-control" name="amount"
                                            value="{{ old('amount')?old('amount'):'' }}" placeholder="XXX.XX">
                                        @if($errors->has('amount'))
                                        <span class="text-danger">
                                            {{ $errors->first('amount') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="discount">Discount (Rs)</label>
                                        <input type="number" id="discount" class="form-control" name="discount"
                                            value="{{ old('discount')?old('discount'):'' }}" placeholder="XXX.XX">
                                        @if($errors->has('discount'))
                                        <span class="text-danger">
                                            {{ $errors->first('discount') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="total_amount">Total Amount(Rs)</label>
                                        <input type="number" id="total_amount" class="form-control" name="total_amount"
                                            value="{{ old('total_amount')?old('total_amount'):'' }}" placeholder="XXX.XX">
                                        @if($errors->has('total_amount'))
                                        <span class="text-danger">
                                            {{ $errors->first('total_amount') }}
                                        </span>
                                        @endif
                                    </div>
                                </div>
                            </div>                                                                                    
                        </div>
                        <!-- /.card -->
                    </div>
                </div>

                <div class="col-md-3">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Action</h3>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Submit">
                                <a href="{{ route('sale.index') }}" class="btn btn-danger">Cancel</a>
                            </div>
                        </div>
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </form>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
@endsection

@section('script')

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {
        $('#customer_name, #product_category, #product_id').select2({
            theme: 'bootstrap4'
        });
        
        $('#product_category').on('change', function(e){            
            var product_category_id = e.target.value;  
            $('#price').val(''); 
            $('#quantity').val(''); 
            $('#amount').val(''); 
            $('#discount').val(''); 
            $('#total_amount').val(''); 

            $.ajax({                       
                url:"{{ route('ajax.sub_category') }}",            
                type:"POST",
                data: {
                    product_category_id: product_category_id
                },            
                success:function (data) {
                    $('#product_id').empty();
                    $('#product_id').append('<option selected="selected">Choose product below...</option>');                        
                    $.each(data.product,function(index,value){                
                        $('#product_id').append('<option value="'+value.id+'">'+value.name+'</option>');                        
                    });
                }
            });
        });

        
        $('#product_id').on('change', function(e){      
            $('#price').val(''); 
            $('#quantity').val(''); 
            $('#amount').val(''); 
            $('#discount').val(''); 
            $('#total_amount').val('');      
            var product_id = e.target.value;        
            $.ajax({                       
                url:"{{ route('ajax.product_price') }}",            
                type:"POST",
                data: {
                    product_id: product_id
                },            
                success:function (data) {
                    $('#price').val(data.price);
                }
            });
        });


        $('#quantity, #price').on('change keydown keyup keypress', function(e){      
            var price = 0;
            var quantity = 0;
            var amount = 0;
            var price = $('#price').val();
            var quantity = $('#quantity').val();
            var amount = price * quantity;
            $('#amount').val(amount);
        });

        $('#discount, #amount').on('change keydown keyup keypress', function(e){      
            var amount = 0;
            var discount = 0;
            var total_amount = 0;
            var amount = $('#amount').val();
            var discount = $('#discount').val();
            var total_amount = amount - discount;
            $('#total_amount').val(total_amount);
        });
    });
</script>
@endsection