@extends('backend.layouts.master')

@section('title')
Manage User
@endsection
@section('content')
<section class="content">
    @include('flashMessage.message')
    <div class="card">
        <div class="card-header">
            Users
        </div>
        <div class="card-body">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">S.N</th>
                        <th scope="col">User</th>
                        <th scope="col">Email</th>
                        <th scope="col">Role</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse($users as $value)
                    <tr>
                        <td>{{$loop->iteration}}</td>
                        <td>{{$value->name}}</td>
                        <td>{{$value->email}}</td>
                        <td>
                            <label class="badge badge-success">{{ $value->getRoleNames() }}</label>
                        </td>
                        <td>
                            <form class="form-inline" method="post"
                                action="{{ route('user.destroy', $value->id) }}">
                                @csrf
                                @method('delete')
                                <a href="{{ route('user.edit', $value->id) }}" class="btn btn-primary m-2 btn-sm"><i
                                        class="fa fa-edit"> </i></a>
                                    @if (Auth::user()->id != $value->id)
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm">                                        
                                        @else
                                        <button onclick="return confirm('Are you sure?')" type="submit" class="btn btn-danger btn-sm" disabled>                                                                                
                                    @endif
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">
                            No data found!
                        </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</section>
@stop
