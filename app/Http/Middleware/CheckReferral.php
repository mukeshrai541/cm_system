<?php

namespace App\Http\Middleware;

use App\Model\Customer;
use Closure;

class CheckReferral
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        
        $referral_id = $request->customer->referral_id;

        if( $user->hasRole(['admin', 'editor', 'data-entry']) ){
            return $next($request);            
        }elseif( $user->hasRole('data-entry') && $referral_id == $user->id ){
            return $next($request);            
        }else{
            return redirect()->back()->with('error_msg', 'You do not have that roles.');
        }

    }
}
