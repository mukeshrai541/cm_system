<?php

namespace App\Http\Controllers;

use App\Model\Customer;
use Illuminate\Http\Request;

class CustomerLocationController extends Controller
{
    public function index(Request $request)
    {                
        $count = Customer::count();
        if( $count > 0)        
        {
            $all_location = Customer::pluck('address');
        }else{
            $all_location = array('kathmandu', 'bhaktapur', 'lalitpur');
        }
        return view('backend.customer_location.index', compact('all_location'));
    }     
}
