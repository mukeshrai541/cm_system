<?php

namespace App\Http\Controllers;

use App\Model\Customer;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'role:admin|editor|data-entry', 'CheckReferral'], ['only' => [
            'edit',
        ]]);

    }
    
    public function index(Request $request)
    {
        if($request->ajax())
        {
            
            $user = auth()->user();

            if( $user->hasRole(['admin', 'editor']) ){
                
                $data = Customer::latest()->get();
                
            }else{
                
                $data = Customer::latest()->mine()->get();

            }

            return Datatables::of($data)
                    ->addColumn('action', function($data){
                        $button = '<a  class="btn btn-primary btn-sm" href="'.route('customer.edit', $data->id).'"><i class="fa fa-edit"></i></a>';
                        $button .= '<button class="btn btn-danger btn-sm mx-1" id="'.$data->id.'" data-toggle="modal"
                        data-target="#deletePackageModal" onclick="deleteData('.$data->id.')"><i class="fa fa-trash"></i></button>';                
                        return $button;                
                    })
                    ->editColumn('created_at', function($data){
                        return date('Y-m-d', strtotime($data->created_at) );
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
        }

        return view('backend.customer.index');
    }

    public function create()
    {
        return view('backend.customer.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
			'email' => 'required|email|unique:customers,email',
		]);    
			
        // dd($request->all());
        if(auth()->check()){
            $input = $request->all();
            $input['referral_id'] = auth()->user()->id;
            Customer::create($input);
        }else{
            return redirect()->back()->with('error_msg', 'OOps! User referral id not found.')        ;
        }
        return redirect()->back()->with('success_msg', 'User Created Successfully');
        
    }

    public function show(Customer $customer)
    {
        //
    }

    public function edit(Customer $customer)
    {
        return view('backend.customer.edit', compact('customer'));
    }

    public function update(Request $request, Customer $customer)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:customers,email,'.$customer->id,
        ]);

        $input = $request->all();
        $input['referral_id'] = auth()->user()->id;
        $customer->update($input);
        return redirect()->back()->with('success_msg', 'Customer Updated Successfully.');
        
    }

    public function destroy(Customer $customer)
    {
        $customer->delete();
        return redirect()->back()->with('success_msg', 'Customer Deleted Successfully');
    }
}
