<?php

namespace App\Http\Controllers;

use App\Model\Sale;
use App\Model\Customer;
use App\Model\Product;
use App\Model\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;


class SaleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(['auth', 'role:admin|editor'], ['only' => [
            'edit','update', 'destroy'
        ]]);
    }

    public function index(Request $request)
    {
        if($request->ajax())
        {
            
            $user = Auth::user();

            if( $user->hasRole(['admin', 'editor']) ){
                
                $data = Sale::with('product')->latest()->get();

                return Datatables::of($data)
                    ->addColumn('action', function($data){
                        $button = '<a  class="btn btn-primary btn-sm" href="'.route('sale.edit', $data->id).'"><i class="fa fa-edit"></i></a>';
                        $button .= '<button class="btn btn-danger btn-sm mx-1" id="'.$data->id.'" data-toggle="modal"
                        data-target="#deletePackageModal" onclick="deleteData('.$data->id.')"><i class="fa fa-trash"></i></button>';                
                        return $button;                
                    })
                    ->editColumn('created_at', function($data){
                        return date('Y-m-d h:i:sa', strtotime($data->created_at) );
                    })
                    ->editColumn('product_name', function($data){
                        return $data->product->name;
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
                
            }else{
                
                $data = Sale::with('product')->latest()->mine()->get();

                return Datatables::of($data)
                    ->addColumn('action', function($data){
                        $button = '<button  class="btn btn-primary btn-sm" disabled ><i class="fa fa-edit"></i></button>';
                        $button .= '<button class="btn btn-danger btn-sm mx-1" id="'.$data->id.'" disabled><i class="fa fa-trash"></i></button>';                
                        return $button;                
                    })
                    ->editColumn('created_at', function($data){
                        return date('Y-m-d h:i:sa', strtotime($data->created_at) );
                    })
                    ->editColumn('product_name', function($data){
                        return $data->product->name;
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);

            }

            
        }

        return view('backend.sale.index');
    }

    public function create()
    {
        $customer = Customer::asc_name()->get();
        $product_category = ProductCategory::asc()->get();

        return view('backend.sale.create', 
        compact(
            'customer',
            'product_category'
        ));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'customer_name' => 'required',
            'product_category' => 'required|integer',
            'product_id' => 'required|integer',
            'price' => 'required', 'regex:/^\d*(\.\d{2})?$/',
            'quantity' => 'required', 'regex:/^\d*(\.\d{2})?$/',
            'amount' => 'required', 'regex:/^\d*(\.\d{2})?$/',
            'discount' => 'required', 'regex:/^\d*(\.\d{2})?$/',
            'total_amount' => 'required', 'regex:/^\d*(\.\d{2})?$/'
        ]);
        // dd($request->all());

        try{
            $input = $request->all();
            $input['user_id'] = Auth::user()->id;
            Sale::create($input);                
        }catch( \Exception $e){
            return redirect()->back()->with('error_msg', 'Oops! Sale not recorded.');
        }
        return redirect()->back()->with('success_msg', 'Sales recorded successfully.');
        
    }

    public function show(Sale $sale)
    {
        //
    }

    public function edit($id)
    {
        $sale = Sale::with('product')->where('id', $id)->first();
        $customer = Customer::asc_name()->get();
        $product = Product::latest()->get();

        return view('backend.sale.edit', 
        compact(
            'customer',
            'product',
            'sale'
        ));
    }

    public function update(Request $request, Sale $sale)
    {
        $this->validate($request,[
            'customer_name' => 'required',
            'product_id' => 'required|integer',
            'price' => 'required', 'regex:/^\d*(\.\d{2})?$/',
            'quantity' => 'required', 'regex:/^\d*(\.\d{2})?$/',
            'amount' => 'required', 'regex:/^\d*(\.\d{2})?$/',
            'discount' => 'required', 'regex:/^\d*(\.\d{2})?$/',
            'total_amount' => 'required', 'regex:/^\d*(\.\d{2})?$/'
        ]);
        // dd($request->all());

        try{
            $input = $request->all();
            $input['user_id'] = Auth::user()->id;
            $sale->update($input);                
        }catch( \Exception $e){
            return redirect()->back()->with('error_msg', 'Oops! Sale not recorded.');
        }
        return redirect()->back()->with('success_msg', 'Sales recorded successfully.');
    }

    public function destroy(Sale $sale)
    {
        $sale->delete();
        return redirect()->back()->with('success_msg', 'Sales Deleted successfully.');
    }   
    
    
}
