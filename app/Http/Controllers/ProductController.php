<?php

namespace App\Http\Controllers;

use App\Model\Product;
use App\Model\ProductCategory;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class ProductController extends Controller
{
    public function index(Request $request)
    {
        if($request->ajax())
        {
            
            $data = Product::with('product_category')->latest()->get();

            return Datatables::of($data)
                    ->addColumn('action', function($data){
                        $button = '<a  class="btn btn-primary btn-sm" href="'.route('product.edit', $data->id).'"><i class="fa fa-edit"></i></a>';
                        $button .= '<button class="btn btn-danger btn-sm mx-1" id="'.$data->id.'" data-toggle="modal"
                        data-target="#deletePackageModal" onclick="deleteData('.$data->id.')"><i class="fa fa-trash"></i></button>';                
                        return $button;                
                    })
                    ->editColumn('category', function($data){
                        return $data->product_category->name;
                    })                    
                    ->editColumn('created_at', function($data){
                        return date('Y-m-d', strtotime($data->created_at) );
                    })
                    ->rawColumns(['action'])
                    ->addIndexColumn()
                    ->make(true);
        }

        return view('backend.product.index');
    }

    public function create()
    {
        $product_categories = ProductCategory::asc()->get();
        return view('backend.product.create', compact('product_categories'));
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'price' => 'required|integer',
            'product_category_id' => 'required|integer'
        ]);

        Product::create($request->all());
        return redirect()->back()->with('success_msg', 'Product Added Successfully.');
    }

    public function show(Product $product)
    {
        //
    }

    public function edit(Product $product)
    {
        $product_categories = ProductCategory::asc()->get();
        return view('backend.product.edit', compact('product', 'product_categories'));
    }

    public function update(Request $request, Product $product)
    {
        $this->validate($request,[
            'name' => 'required|string|max:191',
            'price' => 'required|integer',
            'product_category_id' => 'required|integer'
        ]);

        $product->update($request->all());
        return redirect()->back()->with('success_msg', 'Product Updated Successfully.');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return redirect()->back()->with('success_msg', 'Product Deleted Successfully.');
    }
}
