<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
	{		
		$auth_users = auth()->user();
		if(  $auth_users->hasAnyRole(['admin', 'editor'])  ){
			
			$users = User::with('roles')
			->latest()
			->get();

		}
		
		// dd($user);
		return view('backend.user.index', compact('users'));
	}

	public function create()
	{
		$roles = Role::all();
		// dd($roles);
		return view('backend.user.create', compact('roles'));
	}

	public function store(Request $request)
	{
		$this->validate($request, [
			'name' => 'required',
			'email' => 'required|email|unique:users,email',
			'password' => 'required|min:6|confirmed',
			'user_role' => 'required',
			]);    
			
		// dd($request->all());

		$user_id = Auth::user()->id;
		$input = $request->all();
		$input['password'] = Hash::make($input['password']);
		$input['referral_id'] = $user_id;
        $user = User::create($input);
		
		$user->assignRole($input['user_role']);
		return redirect()->back()->with('success_msg', 'User Created Successfully.');
	}

	public function edit($id)
	{
		$user = User::findOrFail($id);
		$roles = Role::all();
		$user_role_array = $user->getRoleNames()->toArray();		
		// dd($user_role_array);
		return view('backend.user.edit', compact('user', 'roles', 'user_role_array'));
	}

	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'name' => 'required|string',
			'email' => 'required|unique:users,email,'.$id,
			'user_role' => 'required',
			]);    
			
		// dd($request->all());
		$input = $request->all();
		if(!empty($request['password'])){
			$input['password'] = Hash::make($request->password);
		}else{
			unset($input['password']);
		}
		$user = User::findOrFail($id);
		$user->update($input);
		DB::table('model_has_roles')->where('model_id',$id)->delete();
		$user->assignRole($request->user_role);
		return redirect()->back()->with('success_msg', 'User Updated Successfully.');
	}

	public function destroy($id)
	{
		$user = User::findOrFail($id);
		$user->delete();
		return redirect()->back()->with('success_msg', 'User Updated Successfully.');
	}
}
