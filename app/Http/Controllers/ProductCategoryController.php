<?php

namespace App\Http\Controllers;

use App\Model\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productCategory = ProductCategory::asc()->get();
        return view('backend.productCategory.index', compact('productCategory'));
    }

    public function create()
    {
        return view('backend.productCategory.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'order' => 'nullable|integer|max:100',
        ]);

        $input = $request->all();
        if( empty($input['order']) ){
            $order = ProductCategory::max('order');
            $input['order'] = $order + 1;
        }
        ProductCategory::create($input);
        return redirect()->back()->with('success_msg', 'New Category Added Successfully.');
    }

   
    public function show(ProductCategory $productCategory)
    {
        //
    }

    public function edit(ProductCategory $productCategory)
    {
        return view('backend.productCategory.edit', compact('productCategory'));
    }

    public function update(Request $request, ProductCategory $productCategory)
    {
        $this->validate($request, [
            'name' => 'required',
            'order' => 'nullable|integer|max:100',
        ]);

        $input = $request->all();
        if( empty($input['order']) ){
            $order = ProductCategory::max('order');
            $input['order'] = $order + 1;
        }
        $productCategory->update($input);
        return redirect()->back()->with('success_msg', 'Category Updated Successfully.');
    }

    
    public function destroy(ProductCategory $productCategory)
    {
        $productCategory->delete();
        return redirect()->back()->with('success_msg', 'Category Deleted Successfully');
    }
}
