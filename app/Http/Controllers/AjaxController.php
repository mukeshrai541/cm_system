<?php

namespace App\Http\Controllers;

use App\Model\Customer;
use App\Model\Product;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class AjaxController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sub_category(Request $request)
    {
        $product_category_id = $request->product_category_id;
        if( $request->ajax() ){
            $product = Product::where('product_category_id',$product_category_id)->get();

            return response()->json([
                'product' => $product
            ]);
        }
    }
    
    public function product_price(Request $request)
    {
        $product_id = $request->product_id;
        if( $request->ajax() ){
            $product = Product::where('id',$product_id)->first();
            
            $price = $product->price;
            return response()->json([
                'price' => $price
            ]);
        }
    }

    public function customer_by_location(Request $request)
    {
        if($request->ajax())
        {
            $data = Customer::where('address', $request->location)->get();
            return Datatables::of($data)
            ->addColumn('action', function($data){
                $button = '<a  class="btn btn-primary btn-sm" href="'.route('home').'"><i class="fa fa-eye"></i>Statements</a>';
                return $button;                
            })
            ->editColumn('created_at', function($data){
                return date('Y-m-d', strtotime($data->created_at) );
            })
            ->rawColumns(['action'])
            ->addIndexColumn()
            ->make(true);
        }
    }
}
