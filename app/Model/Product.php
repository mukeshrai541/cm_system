<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\ProductCategory;
use App\Model\Sale;

class Product extends Model
{
    protected $table = 'products';
    protected $fillable = ['name', 'price', 'product_category_id'];

    public function product_category()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function sale()
    {
        return $this->hasMany(Sale::class);
    }
}
