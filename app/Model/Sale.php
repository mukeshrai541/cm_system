<?php

namespace App\Model;

use App\User;
use App\Model\Product;
use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $table = 'sales';
    protected $fillable =[
        'customer_name',
        'quantity',
        'price',
        'amount',
        'discount',
        'total_amount',
        'product_id',
        'user_id'
    ];
    
    public function users()
    {
        return $this->belongsTo(User::class);
    }
    public function product()
    {
        return $this->belongsTo(Product::class);
    }
    public function scopeMine($query)
    {
        return $query->where('user_id', auth()->user()->id);
    }


}
