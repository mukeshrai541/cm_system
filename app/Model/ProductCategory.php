<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Product;

class ProductCategory extends Model
{
    protected $table = 'product_categories';
    protected $fillable = ['name', 'order'];

    public function products()
    {
        return $this->hasMany(Product::class);
    }
    
    public function scopeAsc($query)
    {
        return $query->orderBy('order', 'asc');
    }
}
