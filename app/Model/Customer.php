<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'customers';
    protected $fillable = [
        'full_name',
        'email',
        'phone_number',
        'dob',
        'address',
        'referral_id'
    ];

    public function scopeMine($query)
    {
        return $query->where('referral_id', auth()->user()->id);
    }
    
    public function scopeAsc_name($query)
    {
        return $query->orderBy('full_name', 'asc');
    }
}
