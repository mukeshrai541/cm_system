<?php

namespace App;

use App\Model\Sale;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;

    use Notifiable;

    protected $fillable = [
        'name', 'email', 'password', 'referral_id', 'address', 'dob', 'phone_number'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];    

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }
}