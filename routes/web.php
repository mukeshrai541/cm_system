<?php

use App\Http\Controllers\SaleController;

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware' => ['auth']], function () {
	Route::get('/', 'Backend\BackendController@index')->name('home');	
});
		
Route::prefix('admin')->group(function () {
	Route::group(['middleware' => ['auth', 'role:admin']], function () {
		Route::resource('user', 'UserController');
	});
	
});

Route::resource('customer', 'CustomerController', ['names' => 'customer']);

Route::resource('product_category', 'ProductCategoryController', ['names' => 'product_category']);

Route::resource('product', 'ProductController', ['names' => 'product']);

Route::resource('sale', 'SaleController', ['names' => 'sale']);

Route::post('ajax/sub_category', 'AjaxController@sub_category')->name('ajax.sub_category');
Route::post('ajax/product_price', 'AjaxController@product_price')->name('ajax.product_price');
Route::post('ajax/customer_by_location', 'AjaxController@customer_by_location')->name('ajax.customer_by_location');


Route::get('customer_by_location', 'CustomerLocationcontroller@index')->name('customer_by_location.index');